; This file describes the core project requirements for Photo. Several
; patches against Drupal core and their associated issue numbers have been
; included here for reference.

api = 2
core = 7.x

projects[drupal][type] = core
projects[drupal][version] = 7.23

; ------- installation profile --------

; Download the Photo video install profile
projects[openfolio][type] = profile
projects[openfolio][download][type] = git
projects[openfolio][download][url] = https://bitbucket.org/oseds/profile-photo.git
;projects[openfolio][download][branch] = 7.x-1.3
