api = 2
core = 7.x

; Modules
projects[views_bulk_operations][subdir] = "contrib"
projects[views_bulk_operations][version] = "3.1"

projects[admin_menu][subdir] = "contrib"
projects[admin_menu][version] = "3.0-rc4"

projects[advanced_help][subdir] = "contrib"
projects[advanced_help][version] = "1.0"

projects[auto_entitylabel][subdir] = "contrib"
projects[auto_entitylabel][version] = "1.2"

projects[backup_migrate][subdir] = "contrib"
projects[backup_migrate][version] = "2.7"

projects[browscap][subdir] = "contrib"
projects[browscap][version] = "2.0"

projects[browscap_ctools][subdir] = "contrib"
projects[browscap_ctools][version] = "1.0"

projects[bulk_media_upload][subdir] = "contrib"
projects[bulk_media_upload][version] = "1.x-dev"

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.3"

projects[ccl][subdir] = "contrib"
projects[ccl][version] = "1.5"

projects[colorbox][subdir] = "contrib"
projects[colorbox][version] = "2.4"

projects[devel][subdir] = "contrib"
projects[devel][version] = "1.3"

projects[diff][subdir] = "contrib"
projects[diff][version] = "3.2"

projects[draggableviews][subdir] = "contrib"
projects[draggableviews][version] = "2.0"

projects[entity][subdir] = "contrib"
projects[entity][version] = "1.2"

projects[entityreference][subdir] = "contrib"
projects[entityreference][version] = "1.0"

projects[entityreference_prepopulate][subdir] = "contrib"
projects[entityreference_prepopulate][version] = "1.3"

projects[entityreference_view_widget][subdir] = "contrib"
projects[entityreference_view_widget][version] = "1.0-alpha2"

projects[features][subdir] = "contrib"
projects[features][version] = "1.0"

projects[libraries][subdir] = "contrib"
projects[libraries][version] = "2.1"

projects[link][subdir] = "contrib"
projects[link][version] = "1.1"

projects[module_filter][subdir] = "contrib"
projects[module_filter][version] = "1.8"

projects[panels][subdir] = "contrib"
projects[panels][version] = "3.3"

projects[pathauto][subdir] = "contrib"
projects[pathauto][version] = "1.2"

projects[plupload][subdir] = "contrib"
projects[plupload][version] = "1.3"

projects[rules][subdir] = "contrib"
projects[rules][version] = "2.3"

projects[strongarm][subdir] = "contrib"
projects[strongarm][version] = "2.0"

projects[token][subdir] = "contrib"
projects[token][version] = "1.5"

projects[views][subdir] = "contrib"
projects[views][version] = "3.7"

projects[views_slideshow][subdir] = "contrib"
projects[views_slideshow][version] = "3.0"

projects[uuid][subdir] = "contrib"
projects[uuid][download][type] = git
projects[uuid][download][revision] = 4730c678a5a09ab0529ea3222e6123bd929b8da0
projects[uuid][download][branch] = 7.x-1.x

projects[node_export][subdir] = "contrib"
projects[node_export][version] = "3.0"

; Themes
projects[adaptivetheme][version] = "2.3"
projects[corolla][version] = "2.2"

; Photos
projects[oauth][subdir] = "contrib"
projects[oauth][version] = "3.1"

projects[oauthconnector][subdir] = "contrib"
projects[oauthconnector][version] = "1.0-beta2"

projects[connector][subdir] = "contrib"
projects[connector][version] = "1.0-beta2"

projects[http_client][subdir] = "contrib"
projects[http_client][version] = "2.4"

; Amazon s3
projects[amazons3][subdir] = "contrib"
projects[amazons3][version] = "1.0-beta7"

projects[awssdk][subdir] = "contrib"
projects[awssdk][version] = "5.4"

projects[amazons3_cors][subdir] = "contrib"
projects[amazons3_cors][version] = "1.x-dev"

projects[jquery_update][subdir] = "contrib"
projects[jquery_update][version] = "2.3+9-dev"

; Services
projects[services][subdir] = "contrib"
projects[services][version] = "3.5"

projects[services_views][subdir] = "contrib"
projects[services_views][version] = "1.0-beta2"
